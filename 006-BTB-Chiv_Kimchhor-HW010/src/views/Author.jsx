import React, { useState } from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import { postArticle, uploadImage } from "../services/article_service";
export default function Author() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  return (
    <Container>
      <h1 className="my-2">Author</h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="name">
              <Form.Label>Author Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Author Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="text"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              // onClick={onAdd}
            >
              Submit
            </Button>
          </Form>
        </Col>
        <Col md={4}>
          <img className="w-100" src={imageURL} />
          <Form>
            <Form.Group>
              <Form.File
                id="img"
                label="Choose Image"
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageURL(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>
      <Row>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Image</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
              <td>
                <Button size="sm" variant="warning">
                  Edit
                </Button>{" "}
                <Button size="sm" variant="danger">
                  Delete
                </Button>
              </td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Mark</td>
              <td>Otto</td>
              <td>@mdo</td>
              <td>
                <Button size="sm" variant="warning">
                  Edit
                </Button>{" "}
                <Button size="sm" variant="danger">
                  Delete
                </Button>
              </td>
            </tr>
            
          </tbody>
        </table>
      </Row>
    </Container>
  );
}
